Instructions:

For results of the comparison please go to 
https://cuhbioinformatics.atlassian.net/wiki/spaces/BT/pages/156139533/gene+transcript+list+comparison+between+Sophia+vs+Nirvana

To run the script, type the following

$python3 gene2transcript_comparison.py refseqs_for_sophia_analysis.csv mnt/storage/data/NGS/nirvana_genes2transcripts

this should produce terminal output and a gene2tx.log file containing the results.


