#!usr/bin/python3

import sys
import os
import datetime

"""compare gene/transcript list between refseqs_for_sophia_analysis.csv and nirvana_gene2transcript"""

def compare_sophia2nirvana(sophia, gene2transcript_nirvana):

	dir_path = os.path.dirname(sophia)
	output = os.path.join(dir_path, "genes2tx_log.txt")

	gene = []
	tx = []

	with open (gene2transcript_nirvana, "r") as nirvana_tsv:
		for line in nirvana_tsv:
			n_fields = line.strip().split("\t")
			gene.append(n_fields[0])
			tx.append(n_fields[1])

	nirvana_dict = dict(zip(gene, tx))

	with open (sophia, "r") as sophia_csv:
		for index, line in enumerate(sophia_csv):
			s_fields = line.strip().split(",")
			gene = s_fields[0]
			transcript = s_fields[1]

			if index == 0:
				continue

			elif "and" in s_fields[1]:
				txs = s_fields[1].split(" and ")
				tx1 = txs[0]
				tx2 = txs[1]
				if gene in nirvana_dict:
					if tx1 == nirvana_dict[gene]:
						print ("{}\t{}\tpresent in nirvana_gene2transcript".format(gene, tx1))
						with open(output, "a") as log_file:
							log_file.writelines("{}\t{}\t present\n".format(gene, tx1))
					else:
						print ("{}'s {}\t absent in nirvana_gene2transcript".format(gene, tx1))
						with open(output, "a") as log_file:
							log_file.writelines("{}\tpresent {}\t absent\n".format(gene, tx1))

					if tx2 == nirvana_dict[gene]:
						print ("{}\t{}\tpresent in nirvana_gene2transcript".format(gene, tx2))
						with open(output, "a") as log_file:
							log_file.writelines("{}\t{}\t present\n".format(gene, tx2))
					else:
						print ("{}'s {}\t absent from nirvana_gene2transcript".format(gene, tx2))
						with open(output, "a") as log_file:
							log_file.writelines("{}\tpresent {}\t absent\n".format(gene, tx2))
				else:
					print ("{}\tabsent from nirvana_gene2transcript".format(gene))
					with open(output, "a") as log_file:
							log_file.writelines("{}\tabsent\n".format(gene))

			elif index >= 1:
				if gene in nirvana_dict:
					if transcript == nirvana_dict[gene]:
						print ("{}\t{}\tpresent in nirvana_gene2transcript".format(gene, transcript))
						with open(output, "a") as log_file:
							log_file.writelines("{}\t{}\t present\n".format(gene, transcript))

					else:
						print ("{}'s {}\t absent from nirvana_gene2transcript".format(gene, transcript))
						with open(output, "a") as log_file:
							log_file.writelines("{}\tpresent\t{}\tabsent\n".format(gene, transcript))

				else:
					print ("{}\tabsent from nirvana_gene2transcript".format(gene))
					with open(output, "a") as log_file:
							log_file.writelines("{}\tabsent\n".format(gene))

			else:
				continue

	print ("{}".format(datetime.datetime.today()))
	with open(output, "a") as log_file:
		log_file.writelines("{}\n".format(datetime.datetime.today()))

def main(sophia, nirvana):

	compare_sophia2nirvana(sophia, nirvana)

if __name__ == "__main__":
	main(sys.argv[1], sys.argv[2])










